(require 'package)

(setq package-archives
      '(("gnu" . "http://elpa.gnu.org/packages/")
	("marmalade" . "http://marmalade-repo.org/packages/")
	("melpa" . "http://melpa.milkbox.net/packages/")))
(package-initialize)

;; Add my code to path
(add-to-list 'load-path "~/emacs")

;; Load 'zen mode'
(load "naked.el")
(require 'julia-mode)

;; Ido mode
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)

(load "~/code/ESS/lisp/ess-site.el")
(setq ess-julia-program-name "/home/gowda/bin/julia")
(setq inferior-julia-program-name "/home/gowda/bin/julia")

(set-default-font "Inconsolata-12")
(load-theme 'solarized-dark t)

(require 'org-install)
(org-babel-do-load-languages
 'org-babel-load-languages
 '((sh . true) (python . true) (haskell . true))
 )

;; org-mode key bindings
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c b") 'org-iswitchb)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
