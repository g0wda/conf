mkdir -p /usr/share/fonts/truetype
mkdir -p /usr/share/fonts/opentype
mv -f *.ttf */*.ttf /usr/share/fonts/truetype
mv -f *.otf */*.otf /usr/share/fonts/opentype
cd /usr/share/fonts/truetype
mkfontscale
mkfontdir
fc-cache
xset fp rehash

cd /usr/share/fonts/opentype
mkfontscale
mkfontdir
fc-cache
xset fp rehash

