#!/bin/sh
# Hardlinks a configuration file / directory into
# this repository. Also adds an entry in mapping.db.

source="$1"
dest="$2"

if [ -z "$2" ]; then
    dest=`basename "$source"`
fi

if [ -e $dest ]; then
    echo "That file already exists. y - Overwrite/Merge, any other key - quit"
    read ans
    if [ $ans != y ]; then
	exit 1
    fi
    rm $dest
fi

log_mapping () {
    echo $1 \"$2\" \"$3\" >> mapping.db
}

if [ -d "$source" ]; then
    cp -al "$source" "$dest" &&
    echo Hardlinking files in "$source" to "$dest"
    log_mapping d "$source" "$dest"
elif [ -f "$source" ]; then
    echo Hardlinking "$source" to "$dest"
    ln "$source" "$dest" &&
    log_mapping f "$source" "$dest"
fi
