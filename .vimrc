syntax on
set shiftwidth=4
set tabstop=4
set expandtab
set runtimepath+=$HOME/.vim/plugin
colorscheme wombat
nnoremap <Control-Alt-Space> :Goyo<CR>
